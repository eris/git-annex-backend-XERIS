# ERIS key backend for git-annex
Use [ERIS](https://eris.codeberg.page) read capabilities as git-annex keys.

See the git-annex [backends documentation](https://git-annex.branchable.com/backends/)
for configuration instructions. The backend name is `XERIS` (the first X is
for external).

## Building
Nim doesn't do `-` in module names, so build `git_annex_backend_XERIS` and
rename the binary to `git-annex-backend-XERIS`.
